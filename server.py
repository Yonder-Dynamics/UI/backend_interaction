import socketio
from flask import Flask
from functools import partial
from std_msgs.msg import String, Int32MultiArray, Float64MultiArray, Float32, Int16
from geometry_msgs.msg import Pose, PoseArray, PoseStamped
import ros_handler

sio = socketio.Server(async_mode="threading")
app = Flask(__name__)
app.debug = True
app.wsgi_app = socketio.WSGIApp(sio, app.wsgi_app)

def on_get(topic, sid, data):
    sio.emit(topic, ros_handler.get(topic))

def add_get_route(topicname, datatype, transform, default):
    sio.on(topicname)(partial(on_get, topicname))

def on_post(topic, sid, data):
    ros_handler.publish(topic, data)

def add_post_route(topicname, datatype, transform, queue_size=1):
    ros_handler.init_pub(topicname, datatype, transform, queue_size)
    sio.on(topicname)(partial(on_post, topicname))

@sio.event
def connect(sid, environ):
    print('connected ', sid)

@sio.event
def disconnect(sid):
    print('disconnected ', sid)

def start():
    app.run(host='0.0.0.0', port=5000, threaded=True)
