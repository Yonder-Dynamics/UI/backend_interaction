#!/usr/bin/zsh

source ~/.zshrc
roscd backend_interaction && gunicorn -b 0.0.0.0:8000 api:app
