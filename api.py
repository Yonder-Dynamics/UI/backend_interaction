import falcon
import math
import json
import base64
import numpy as np
import rospy
from std_msgs.msg import (
    String,
    Int32MultiArray,
    Float64MultiArray,
    MultiArrayLayout,
    MultiArrayDimension,
    Float32MultiArray,
    Int16,
    Empty,
    UInt16,
    Float64,
)
from geometry_msgs.msg import Pose, PoseArray, PoseStamped, Twist
from nav_msgs.msg import Odometry, Path
from sensor_msgs.msg import NavSatFix, JointState
import threading
from science_analyzer.srv import ReadSpectrometer

""" 
API GUIDE

Uses https://falconframework.org/ as the api handler

To run 
pip install gunicorn
gunicorn api:app

PATHS:

POST /drive_train
POST /arm

"""

production = False
ARM = "arm"
DRIVE = "drive"
MANUAL = "manual"
NUM_TURBIDITY = 4
TURBIDITY_BUFFER_LEN = 100

threading.Thread(
    target=lambda: rospy.init_node("movement", anonymous=True, disable_signals=True)
).start()
# rospy.init_node('movement', anonymous=True, disable_signals=True)
drive_pub = rospy.Publisher("manual_drive_train", Float64MultiArray, queue_size=1)
arm_pub = rospy.Publisher("arm_speeds", Float64MultiArray, queue_size=1)
goal_coords_pub = rospy.Publisher("rover_waypoint", Float64MultiArray, queue_size=10)
spectrometer_pub = rospy.Publisher("spectrometerConfig", Int32MultiArray, queue_size=1)
pathdrawing_pub = rospy.Publisher("rover_path", Path, queue_size=1)
pid_tuning_pub = rospy.Publisher("pid_tuning_values", Float64MultiArray, queue_size=1)
zero_encoder_pub = rospy.Publisher("zero_encoder", Int16, queue_size=1)

if not production:
    from falcon_cors import CORS

    public_cors = CORS(
        allow_all_origins=True, allow_methods_list=["GET", "PATCH", "POST", "DELETE"]
    )
    app = falcon.API(middleware=[public_cors.middleware])
else:
    app = falcon.API()


# Gives direction to the drive train
class DriveTrain(object):
    def __init__(self, pub, control_multiplexer, state_handler):
        self.pub = pub
        self.control_multiplexer = control_multiplexer
        self.state_handler = state_handler

    def on_post(self, req, resp):
        if not self.control_multiplexer.is_current_controller(DRIVE, req.access_route):
            resp.status = falcon.HTTP_500  # Error
            resp.body = json.dumps({"message": "someone else grabbed control"})
            return

        if self.state_handler.state != MANUAL:
            resp.status = falcon.HTTP_400  # Error
            resp.body = json.dumps(
                {
                    "message": "rover not in manual. state: "
                    + str(self.state_handler.state)
                }
            )
            return

        query = falcon.uri.decode(req.query_string)
        queries = query.split("&")

        # what all the data for the return package will go into
        package = {}

        try:
            drive_train = {"x": 0, "y": 0}

            # getting all the query parameters out of the POST url
            for each in queries:
                if "=" in each:
                    name, data = each.split("=")
                    if name in drive_train:
                        drive_train[name] = float(data)

            x = drive_train["x"]
            y = drive_train["y"]

            if x**2 + y**2 == 0:
                left = 0
                right = 0
            else:
                magnitude = max((abs(y) + abs(x)) / math.sqrt(x**2 + y**2), 1e-9)
                left = int(255 * (y + x) / magnitude)
                right = int(255 * (y - x) / magnitude)

            message = Float64MultiArray()
            message.data = [left, right]

            self.pub.publish(message)

            resp.status = falcon.HTTP_200  # This is the default status
            resp.body = json.dumps({"message": "successfully published"})

        except Exception as e:
            resp.status = falcon.HTTP_500  # Error
            # resp.body = (json.dumps({"message":"something failed, check error", "error":e}))


# Gives endpoints for the arm
class Arm(object):
    def __init__(self, pub, control_multiplexer, state_handler):
        self.pub = pub
        self.control_multiplexer = control_multiplexer
        self.state_handler = state_handler

    def on_post(self, req, resp):
        if not self.control_multiplexer.is_current_controller(ARM, req.access_route):
            resp.status = falcon.HTTP_500  # Error
            resp.body = json.dumps({"message": "someone else grabbed control"})
            return

        if self.state_handler.state != MANUAL:
            resp.status = falcon.HTTP_400  # Error
            resp.body = json.dumps(
                {
                    "message": "rover not in manual. state: "
                    + str(self.state_handler.state)
                }
            )
            return

        query = falcon.uri.decode(req.query_string)
        queries = query.split("&")

        # what all the data for the return package will go into
        package = {}

        try:

            arm = {
                "x": 0,
                "y": 0,  # up down
                "z": 0,
                "x_angle": 0,  # side by side
                "y_angle": 0,  # up down
                "rotation": 0,  # spinning back and forth
            }

            # getting all the query parameters out of the POST url
            for each in queries:
                if "=" in each:
                    name, data = each.split("=")
                    if name in arm:
                        arm[name] = float(data)

            message = Float64MultiArray()
            message.data = [
                arm["x"],
                arm["z"],
                arm["y"],
                arm["x_angle"],
                arm["y_angle"],
                arm["rotation"],
            ]

            self.pub.publish(message)

            resp.status = falcon.HTTP_200  # This is the default status
            resp.body = json.dumps({"message": "successfully published"})

        except Exception as e:
            resp.status = falcon.HTTP_500  # Error
            resp.body = json.dumps(
                {"message": "something failed, check error", "error": e.error}
            )


# Gives endpoints for the arm
class Imu(object):
    def __init__(self):
        self.sub = rospy.Subscriber("imu", String, self.update_data)
        self.imu_data = "data"

    def update_data(self, imu_data):
        self.imu_data = imu_data.data

    def on_get(self, req, resp):
        query = falcon.uri.decode(req.query_string)

        resp.status = falcon.HTTP_200  # This is the default status
        resp.body = self.imu_data


# Gives endpoints for tag detection
# class TagDetector(object):
# def __init__(self):
# self.sub = rospy.Subscriber('ar_tag_bbox', String, self.update_bboxes)
# self.sub = rospy.Subscriber('ar_tag_img', CompressedImg, self.update_img)
# self.bbox_centers = []
# self.img = []

# def update_bboxes(self, bboxes):
# self.bbox_centers = [(bboxes.data[i], bboxes.data[i+1]) for i in range(len(bboxes.data))]

# def update_img(self, img):
# np_arr = np.fromstring(ros_data.data, np.uint8)
# self.img = cv2.imdecode(np_arr, cv2.CV_LOAD_IMAGE_COLOR)

# def on_get(self, req, resp):
# query = falcon.uri.decode(req.query_string)

# resp.status = falcon.HTTP_200  # This is the default status
# resp.body = self.imu_data

# Endpoints for spectrometer data
class Spectrometer(object):
    def __init__(self):
        self.spectrometerData = []
        self.sub = rospy.Subscriber("spectrometerData", Int32MultiArray, self.update)

    def update(self, message):
        self.spectrometerData = message.data

    def on_get(self, req, resp):
        query = falcon.uri.decode(req.query_string)

        resp.status = falcon.HTTP_200
        resp.body = json.dumps({"spectrometerData": self.spectrometerData})


# This one doesn't work yet
class SpectrometerConfig(object):
    def __init__(self, pub):
        self.pub = pub

    def on_post(self, req, resp):
        query = falcon.uri.decode(req.query_string)
        queries = query.split("&")

        try:
            config = {"integrationTime": 0, "averages": 0}

            # getting all the query parameters out of the POST url
            for each in queries:
                if "=" in each:
                    name, data = each.split("=")
                    if name in config:
                        config[name] = float(data)

            message = Int32MultiArray()
            message.data = [config["integrationTime"], config["averages"]]

            self.pub.publish(message)

            resp.status = falcon.HTTP_200  # This is the default status
            resp.body = json.dumps({"message": "successfully published"})

        except Exception as e:
            resp.status = falcon.HTTP_500  # Error
            resp.body = json.dumps(
                {"message": "something failed, check error", "error": e}
            )


class ScienceNotes:
    def __init__(self):
        self.science_note_pub = rospy.Publisher("/science_note", String, queue_size=1)

    def on_post(self, req, resp):
        query = falcon.uri.decode(req.query_string)
        queries = query.split("&")

        class Note:
            def __str__(self):
                return f"{self.cup},{self.note}"

        n = Note()
        for query in queries:
            [param, val] = query.split("=")
            if param == "cup":
                n.cup = val
            if param == "note":
                n.note = val

        self.science_note_pub.publish(String(str(n)))
        resp.status = falcon.HTTP_200
        resp.body = json.dumps({"message": "success"})


class ScienceModule:
    def __init__(self):
        self.spin_pub = rospy.Publisher("/science_spin", Empty, queue_size=1)
        self.pos_pub = rospy.Publisher("/science_set_position", Int16, queue_size=1)
        self.sub = rospy.Subscriber("/turbidity", Float32MultiArray, self.update)
        self.data = [[] for i in range(NUM_TURBIDITY)]

    def update(self, message):
        for i in range(NUM_TURBIDITY):
            self.data[i].append(message.data[i])
            if len(self.data[i]) > TURBIDITY_BUFFER_LEN:
                self.data[i].pop(0)

    def on_get(self, req, resp):
        queries = req.query_string.split("&")
        for query in queries:
            [param, val] = query.split("=")
            if param == "turbidity":
                resp.status = falcon.HTTP_200
                resp.body = json.dumps({"turbidity": self.data})
                return
            if param == "spectroscopy":
                print("getting spect")
                rospy.wait_for_service("read_spectrometer")
                read_spectrometer = rospy.ServiceProxy(
                    "read_spectrometer", ReadSpectrometer
                )
                reading = read_spectrometer(int(val))
                resp.status = falcon.HTTP_200
                resp.body = json.dumps({"spectroscopy": reading.data})
                return

    def on_post(self, req, resp):
        queries = req.query_string.split("&")
        for query in queries:
            [param, val] = query.split("=")
            if param == "position":
                self.pos_pub.publish(Int16(int(val)))
            else:
                self.spin_pub.publish(Empty())

        resp.status = falcon.HTTP_200
        resp.body = json.dumps({"message": "success"})


class BatteryData:
    def __init__(self):
        self.sub = rospy.Subscriber("battery_level", Float64MultiArray, self.update)
        self.data = [0, 0, 0, 0, 0, 0]

    def update(self, message):
        self.data = message.data

    def on_get(self, req, resp):
        query = falcon.uri.decode(req.query_string)
        resp.status = falcon.HTTP_200
        resp.body = json.dumps({"batteryLevel": self.data})


class ArmFingyStallFeedback:
    def __init__(self):
        self.stalling = False
        self.sub = rospy.Subscriber("/feedback", Float32MultiArray, self.on_new_msg)

    def on_new_msg(self, msg):
        self.stalling = True if msg.data[-1] else False

    def on_get(self, req, res):
        res.status = falcon.HTTP_200  # success
        res.body = json.dumps({"finger_stalled": self.stalling})


class ArmPose:
    def __init__(self):
        self.joint_names = ["place_holder_joint_names"] * 5
        self.joint_states = [0] * 5
        self.sub = rospy.Subscriber("/joint_states", JointState, self.on_new_msg)

    def on_new_msg(self, msg):
        self.joint_names = msg.name
        self.joint_states = msg.position

    def on_get(self, req, res):
        res.status = falcon.HTTP_200
        res.body = json.dumps(
            {"joint_names": self.joint_names, "joint_states": self.joint_states}
        )


class RoverPose:
    def __init__(self):
        # self.sub = rospy.Subscriber("/zed/zed_node/pose", PoseStamped, self.update)
        self.arsub = rospy.Subscriber("ar_pose", PoseArray, self.updatear)

        self.sub = rospy.Subscriber("/rover_odom", Odometry, self.update)
        # only x and y cause we don't care about z, and we only care about
        # rotation along z axis
        self.pose = {"position": {"x": 0, "y": 0}, "orientation": {"z": 0}}

        self.ar_pose = []

    def quaternion_to_euler(self, orientation):
        x = orientation.x
        y = orientation.y
        z = orientation.z
        w = orientation.w
        t0 = +2.0 * (w * x + y * z)
        t1 = +1.0 - 2.0 * (x * x + y * y)
        roll = math.atan2(t0, t1)
        t2 = +2.0 * (w * y - z * x)
        t2 = +1.0 if t2 > +1.0 else t2
        t2 = -1.0 if t2 < -1.0 else t2
        pitch = math.asin(t2)
        t3 = +2.0 * (w * z + x * y)
        t4 = +1.0 - 2.0 * (y * y + z * z)
        yaw = math.atan2(t3, t4)
        return (yaw, pitch, roll)

    def update(self, message):
        pose = message.pose.pose

        yaw, pitch, roll = self.quaternion_to_euler(pose.orientation)
        self.pose = {
            "position": {"x": pose.position.x / 10, "y": pose.position.y / 10},
            "orientation": {"z": yaw},
        }

    def updatear(self, message):
        self.ar_pose = [
            {"position": {"x": pose.position.x, "y": pose.position.y}}
            for pose in message.poses
        ]

    def on_get(self, req, resp):
        query = falcon.uri.decode(req.query_string)

        resp.status = falcon.HTTP_200
        resp.body = json.dumps({"rover_pose": self.pose, "ar_pose": self.ar_pose})


class PathDrawing:
    def __init__(self, pub):
        self.pub = pub

    def on_post(self, req, resp):

        body = req.stream.read()

        msg = Path()
        msg.header.frame_id = "map"
        msg.header.stamp = rospy.Time.now()

        last_point = PoseStamped()

        for point in json.loads(body.decode("utf-8")):

            toadd = PoseStamped()
            toadd.pose.position.x = point["x"] * 10
            toadd.pose.position.y = point["y"] * 10

            angle = np.arctan2(
                toadd.pose.position.y - last_point.pose.position.y,
                toadd.pose.position.x - last_point.pose.position.x,
            )

            toadd.pose.position.z = angle

            last_point = toadd

            msg.poses.append(toadd)

        # msg = Twist()
        # # msg.header.frame_id = "map"

        # for point in json.loads(body.decode("utf-8")):
        #     msg.linear.x = point["x"] * 10
        #     msg.linear.y = point["y"] * 10

        resp.status = falcon.HTTP_200
        self.pub.publish(msg)


class ControlMultiplexer:
    def __init__(self):
        self.lock = threading.Lock()
        self.current_controllers = {ARM: "", DRIVE: ""}

    def is_current_controller(self, system, ip):
        with self.lock:
            return self.current_controllers[system] == ip

    def on_post(self, req, resp):
        body = req.stream.read()
        queries = req.query_string.split("&")
        for query in queries:
            [param, val] = query.split("=")
            if param == "system" and val in [ARM, DRIVE]:
                with self.lock:
                    self.current_controllers[val] = req.access_route
            else:
                resp.status = falcon.HTTP_500  # Error
                resp.body = json.dumps({"message": "bad params"})

        resp.status = falcon.HTTP_200
        resp.body = json.dumps({"message": "took control"})


class GoalCoords:
    def __init__(self, pub):
        self.pub = pub

    def on_post(self, req, resp):
        query = falcon.uri.decode(req.query_string)
        queries = query.split("&")

        # [[lat, long, should_look_for_ar, id, speed]]
        goal = Float64MultiArray()
        # [[lat,val],[long,val]...]

        msg = {
            "lat": 0.0,
            "long": 0.0,
            "should_look_for_ar": 0.0,
            "id": 0.0,
            "speed": 0.0
        }
        for query in queries:
            [param, val] = query.split("=")
            if param == "lat":
                msg[param] = float(val)
                # goal.linear.x = float(val)
            if param == "long":
               msg[param] = float(val)
               # goal.linear.y = float(val)
            if param == "should_look_for_ar":
                msg[param] = 1.0 if val == "true" else 0.0
            if param == "id":
                msg[param] = float(val)
               #  goal.angular.x = float(val)
            if param == "speed":
                msg[param] = float(val)
               # goal.angular.y = float(val)
        
        goal.data = msg.values()
        
        # layout is 2d array.
        #  goal.layout = MultiArrayLayout()
       # goal.layout.dim.append(MultiArrayDimension())
       # goal.layout.dim.append(MultiArrayDimension())
       # goal.layout.dim[0].label = "waypoints"
       # goal.layout.dim[1].label = "values"
       # goal.layout.dim[0].size = 1
       # goal.layout.dim[1].size = 5
       # goal.layout.dim[0].stride = 5
       # goal.layout.dim[1].stride = 5
       # goal.layout.data_offset = 0
       # goal.data = [msg]
       

        self.pub.publish(goal)

        resp.status = falcon.HTTP_200
        resp.body = json.dumps({"message": "successfully published"})


class GPSCoords:
    def __init__(self):
        self.sub = rospy.Subscriber(
            "mavros/global_position/global", NavSatFix, self.update
        )
        self.data = {"lat": 0, "long": 0}

    def update(self, message):
        self.data["lat"] = message.latitude
        self.data["long"] = message.longitude

    def on_get(self, req, resp):
        query = falcon.uri.decode(req.query_string)
        resp.status = falcon.HTTP_200
        resp.body = json.dumps(self.data)


class State:
    def __init__(self):
        self.sub = rospy.Subscriber("state", String, self.update_state)
        self.pub = rospy.Publisher("set_state", String, queue_size=1)
        self.state = "off"

    def update_state(self, message):
        self.state = message.data

    def on_get(self, _, resp):
        resp.status = falcon.HTTP_200  # This is the default status
        resp.body = json.dumps({"state": str(self.state)})

    def on_post(self, req, resp):
        query = falcon.uri.decode(req.query_string)
        queries = query.split("&")

        for query in queries:
            [param, val] = query.split("=")
            if param == "state":
                self.pub.publish(String(val))
                resp.status = falcon.HTTP_200
                resp.body = json.dumps({"message": "successfully published"})
                return

        resp.status = falcon.HTTP_400
        resp.body = json.dumps({"message": "no state given"})


class ArmControlState:
    state_map = {"off": 0, "droop": 1, "close_loop": 2, "profile" : 3, "inverse": 4}
    state_lst = list(state_map.keys())

    def __init__(self):
        self.pub = rospy.Publisher("arm_mode", Int16, queue_size=1)
        self.state = self.state_lst[0]

    def update_state(self, message):
        self.state = self.state_lst[message.data]

    def on_get(self, _, resp):
        resp.status = falcon.HTTP_200  # This is the default status
        resp.body = json.dumps({"state": self.state})

    def on_post(self, req, resp):
        query = falcon.uri.decode(req.query_string)
        queries = query.split("&")

        for query in queries:
            [param, val] = query.split("=")
            if param == "state":
                self.pub.publish(Int16(self.state_map[val]))
                self.state = val
                resp.status = falcon.HTTP_200
                resp.body = json.dumps({"message": "state set to " + val})
                return

        resp.status = falcon.HTTP_400
        resp.body = json.dumps({"message": "no state given"})

class ArmProfileState:
    state_map = {"home_stow": 1, "operation": 2, "grab_soil": 3, "science_box": 4}
    state_lst = list(state_map.keys())

    def __init__(self):
        self.pub = rospy.Publisher("execute_profile", Int16, queue_size=1)
        self.state = self.state_lst[0]

    def update_state(self, message):
        self.state = self.state_lst[message.data]

    def on_get(self, _, resp):
        resp.status = falcon.HTTP_200  # This is the default status
        resp.body = json.dumps({"state": self.state})

    def on_post(self, req, resp):
        query = falcon.uri.decode(req.query_string)
        queries = query.split("&")

        for query in queries:
            [param, val] = query.split("=")
            if param == "state":
                self.pub.publish(Int16(self.state_map[val]))
                self.state = val
                resp.status = falcon.HTTP_200
                resp.body = json.dumps({"message": "state set to " + val})
                return

        resp.status = falcon.HTTP_400
        resp.body = json.dumps({"message": "no state given"})


class RoverHeading:
    def __init__(self):
        self.sub = rospy.Subscriber(
            "mavros/global_position/compass_hdg", Float64, self.update
        )
        self.data = {"heading": 0}

    def update(self, message):
        self.data["heading"] = message.data

    def on_get(self, req, resp):
        resp.status = falcon.HTTP_200
        resp.body = json.dumps(self.data)


class PIDTuning:
    def __init__(self, pub):
        self.pub = pub
        self.joints = {"Base": 0, "Shoulder": 1, "Elbow": 2}
    
    def on_post(self, req, resp):
        query = falcon.uri.decode(req.query_string)
        queries = query.split("&")
        message = Float64MultiArray()
        data = {query.split("=")[0]: query.split("=")[1] for query in queries}
        message.data = [self.joints[data["joint"]], float(data["P"]), float(data["I"]), float(data["D"])]
        self.pub.publish(message)

        resp.status = falcon.HTTP_200
        resp.body = json.dumps({"message": "successfully published"})


class ZeroEncoder:
    def __init__(self, pub):
        self.pub = pub
        self.joints = {"Base": 0, "Shoulder": 1, "Elbow": 2}
    
    def on_post(self, req, resp):
        query = falcon.uri.decode(req.query_string)
        queries = query.split("&")
        to_zero = queries[0].split("=")[1]
        self.pub.publish(Int16(self.joints[to_zero]))
        
        resp.status = falcon.HTTP_200
        resp.body = json.dumps({"message": "successfully published"})

if not production:
    from falcon_cors import CORS

    public_cors = CORS(
        allow_all_origins=True, allow_methods_list=["GET", "PATCH", "POST", "DELETE"]
    )
    app = falcon.API(middleware=[public_cors.middleware])
else:
    app = falcon.API()


state = State()
armProfileState = ArmProfileState()
armControlState = ArmControlState()
controlMultiplexer = ControlMultiplexer()
drive_train = DriveTrain(drive_pub, controlMultiplexer, state)
arm = Arm(arm_pub, controlMultiplexer, state)
goal_coords = GoalCoords(goal_coords_pub)
imu = Imu()
spectrometer = Spectrometer()
spectrometerConfig = SpectrometerConfig(spectrometer_pub)
pathdrawing = PathDrawing(pathdrawing_pub)
roverpose = RoverPose()
battery_data = BatteryData()
fingy_stalling = ArmFingyStallFeedback()
arm_joint_states = ArmPose()
gps_coords = GPSCoords()
science_module = ScienceModule()
rover_heading = RoverHeading()
science_notes = ScienceNotes()
pid_tuning = PIDTuning(pid_tuning_pub)
zero_encoder = ZeroEncoder(zero_encoder_pub)

app.add_route("/drive_train", drive_train)
app.add_route("/goal_coords", goal_coords)
app.add_route("/arm", arm)
app.add_route("/imu_data", imu)
app.add_route("/spectrometer_data", spectrometer)
app.add_route("/spectrometer_config", spectrometerConfig)
app.add_route("/battery_level", battery_data)
app.add_route("/gps_coords", gps_coords)
app.add_route("/control_multiplexer", controlMultiplexer)
app.add_route("/rover_pose", roverpose)
app.add_route("/path", pathdrawing)
app.add_route("/state", state)
app.add_route("/arm_control_state", armControlState)
app.add_route("/arm_profile_state", armProfileState)
app.add_route("/science", science_module)
app.add_route("/arm_feedback", fingy_stalling)
app.add_route("/arm_joint_states", arm_joint_states)
app.add_route("/rover_heading", rover_heading)
app.add_route("/science_note", science_notes)
app.add_route("/pid_tuning", pid_tuning)
app.add_route("/zero_encoder", zero_encoder)
