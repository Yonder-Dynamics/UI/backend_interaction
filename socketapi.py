import socketio
import math
from std_msgs.msg import String, Int32MultiArray, Float64MultiArray, Float32, Int16
from geometry_msgs.msg import Pose, PoseArray, PoseStamped
import server

# --- UTILITY FUNCTIONS --- 

def quaternion_to_euler(orientation):
    x = orientation.x
    y = orientation.y
    z = orientation.z
    w = orientation.w
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll = math.atan2(t0, t1)
    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch = math.asin(t2)
    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw = math.atan2(t3, t4)
    return (yaw, pitch, roll)

# --- ROS LISTENER TRANSFORMS ---

def identity(message):
    return message.data

def zed_pose_transform(message):
    yaw, pitch, roll = quaternion_to_euler(message.pose.orientation)
    return {
        "position": {
            "x": message.pose.position.x,
            "y": message.pose.position.y
        },
        "orientation": {
            "z": yaw
        }
    }

def ar_poses_transform(message):
    return [{
        "position": {
            "x": pose.position.x,
            "y": pose.position.y
        }
    } for pose in message.poses]

# --- ROS PUBLISHER TRANSFORMS ---

def drive_train_transform(data):
    x, y = data["x"], data["y"]
    if x == 0 and y == 0:
        left = 0
        right = 0
    else:
        magnitude = max((abs(y) + abs(x)) / math.sqrt(x**2 + y**2), 1e-9)
        left = int(255 * (y + x) / magnitude)
        right = int(255 * (y - x) / magnitude)

    message = Float64MultiArray()
    message.data = [left, right]
    return message

def arm_transform(data):
    message = Float64MultiArray()
    message.data = [data["x"], data["y"], data["z"],
                    data["x_angle"], data["y_angle"],
                    data["rotation"]]
    return message

def spectrometer_config_transform(data):
    message = Int32MultiArray()
    message.data = [data["integrationTime"], data["averages"]]
    return message

def pathdrawing_transform(data):
    message = PoseArray()
    message.header.frame_id = "map"

    for point in data:
        toadd = Pose()
        toadd.position.x = point['x']
        toadd.position.y = point['y']
        message.poses.append(toadd)
    return message

# --- ROS SUBSCRIBERS ---

server.add_get_route('spectrometerData', Int32MultiArray, identity, [])
server.add_get_route('battery_level', Float64MultiArray, identity, [0, 0, 0, 0, 0, 0])
server.add_get_route('imu', String, identity, "data")
server.add_get_route('ar_pose', PoseArray, ar_poses_transform, [])
server.add_get_route('/zed/zed_node/pose', PoseStamped, zed_pose_transform,
    {
        "position": {
            "x": 0,
            "y": 0
        },
        "orientation": {
            "z": 0
        }
    }
)

# --- ROS PUBLISHERS ---

server.add_post_route('manual_drive_train', Float64MultiArray, drive_train_transform, 10)
server.add_post_route('drive_train_multiplexer', Int16, drive_train_transform)
server.add_post_route('arm_position', Float64MultiArray, arm_transform, 10)
server.add_post_route('spectrometerConfig', Int32MultiArray, spectrometer_config_transform)
server.add_post_route('/frontend/path', PoseArray, pathdrawing_transform)

server.start()
