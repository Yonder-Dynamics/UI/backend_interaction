import rospy
from functools import partial
from std_msgs.msg import String, Int32MultiArray, Float64MultiArray, Float32, Int16
from geometry_msgs.msg import Pose, PoseArray, PoseStamped

rospy.init_node('movement', anonymous=True, disable_signals=True)

# creating subs
roscache = {}

def update_data(topic, transform, message):
    roscache[topic] = transform(message)

def listen(topic, datatype, transform, default):
    rospy.Subscriber(topic, datatype, partial(update_data, topic, transform))
    roscache[topic] = default

def get(topic):
    return roscache[topic]

# creating pubs
rospubs = {}
def init_pub(topic, datatype, transform, queue_size):
    rospubs[topic] = (
        rospy.Publisher(topic, datatype, queue_size=queue_size),
        transform
    )

def publish(topic, data):
    pub, transform = rospubs[topic]
    pub.publish(transform(data))

